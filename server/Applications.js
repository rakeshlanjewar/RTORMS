const express = require('express')
var mongoose = require('mongoose');
var router = express.Router();


var LLApplicationSchema = new mongoose.Schema({
    appNo: { type: String, required: true },
    appDate: Date,
    name: { type: String, required: true, uppercase: true },
    mobNumber: Number,
    DOB: Date,
    slotdate: Date,
    type: String,
    rto: String,
    remark: String
});


var DLApplicationSchema = new mongoose.Schema({
    appNo: { type: String },
    appDate: Date,
    name: { type: String, required: true, uppercase: true },
    mobNumber: Number,
    DOB: Date,
    slotdate: Date,
    type: String,
    llnumber: { type: String, uppercase: true },
    remark: String
});

var dlEntrySchema = new mongoose.Schema({
    dlnumber: { type: String, required: true },
    dldate: Date,
    dlexpiry: Date,
    name: { type: String, required: true },
    mobNumber: Number
});


var ApplicationModel = mongoose.model('LLApplications', LLApplicationSchema);

var DLApplicationModel = mongoose.model('DLApplications', DLApplicationSchema);

var DLEntries = mongoose.model('DLEntries', dlEntrySchema);

router.post('/newAppForm', function (req, res) {
    var entry = new ApplicationModel(req.body);
    entry.save(function (err) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            res.json("Saved");
        }
    });
})

router.delete('/deleteApplication', function (req, res) {
    console.log("Hit /deleteApplication")
    var id = req.body._id;
    ApplicationModel.deleteOne({ _id: id }, function (err) {
        if (err) {
            res.status(500).send("Failed");
        }
        else {
            res.json("Deleted");
        }
    });
})


router.post('/updateApplication', function (req, res) {
    console.log("Hit /updateDLentry")
    var id = req.body._id;
    ApplicationModel.updateOne({ _id: id }, req.body, function (err) {
        if (err) {
            res.status(500).send("Failed");
        }
        else {
            res.json("Updated");
        }
    });
})

router.get('/getApplications', function (req, res) {
    ApplicationModel.find({}, function (err, apps) {
        if (err) {
            res.status(500).json("Failed");
        }
        res.json(apps);
    });
})

///////////////////////////////////////////////////////

router.post('/newDLAppForm', function (req, res) {
    var entry = new DLApplicationModel(req.body);
    entry.save(function (err) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            res.json("Saved");
        }
    });
})

router.delete('/deleteDLApplication', function (req, res) {
    console.log("Hit /deleteApplication")
    var id = req.body._id;
    DLApplicationModel.deleteOne({ _id: id }, function (err) {
        if (err) {
            res.status(500).send("Failed");
        }
        else {
            res.json("Deleted");
        }
    });
})


router.post('/updateDLApplication', function (req, res) {
    console.log("Hit /updateDLentry")
    var id = req.body._id;
    DLApplicationModel.updateOne({ _id: id }, req.body, function (err) {
        if (err) {
            res.status(500).send("Failed");
        }
        else {
            res.json("Updated");
        }
    });
})

router.get('/getDLApplications', function (req, res) {
    DLApplicationModel.find({}, function (err, apps) {
        if (err) {
            res.status(500).json("Failed");
        }
        res.json(apps);
    });
})



router.post('/newDL', function (req, res) {

    var entry = new DLEntries(req.body);
    entry.save(function (err) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            res.json("Saved");
        }
    });
})

router.delete('/deleteDLentry', function (req, res) {
    var id = req.body._id;
    DLEntries.deleteOne({ _id: id }, function (err) {
        if (err) {
            res.status(500).json("Failed");
        }
        else {
            res.json("Deleted");
        }
    });
})


router.post('/updateDLentry', function (req, res) {
    console.log("Hit /updateDLentry")
    var id = req.body._id;
    DLEntries.updateOne({ _id: id }, req.body, function (err) {
        if (err) {
            res.status(500).send("Failed");
        }
        else {
            res.json("Updated");
        }
    });
})


router.get('/getDLentries', function (req, res) {
    DLEntries.find({}, function (err, result) {
        if (err) {
            res.status(500).send("Failed");
        }
        res.json(result);
    });
})

router.post('/LLtoDL', function (req, res) {
    req.body.appNo = "";
    req.body.appDate = "";
    var id = req.body._id;
    delete req.body._id;
    delete req.body.slotdate;
    if (req.body.llnumber == "" || req.body.llnumber == null) {
        res.statusMessage = "Learning License Number is Required"
        res.status(500).json();
    }
    else {
        var entry = new DLApplicationModel(req.body);
        entry.save(function (err, savedData) {
            if (err) {
                res.status(500).json(err);
            }
            else {
                ApplicationModel.deleteOne({ _id: id }, function (err) {
                    if (err) {
                        DLApplicationModel.deleteOne({ _id: savedData.id }, function (err) { });
                        res.status(500).send("Failed");
                    }
                    else {
                        res.json("Success");
                    }
                });
            }
        });
    }
})


router.post('/DLtoDLEntry', function (req, res) {
    var id = req.body._id;
    var entry = new DLEntries(req.body);
    entry.save(function (err, savedData) {
        if (err) {
            res.status(500).json(err);
        }
        else {
            DLApplicationModel.deleteOne({ _id: id }, function (err) {
                if (err) {
                    DLEntries.deleteOne({ _id: savedData.id }, function (err) { });
                    res.status(500).send("Failed");
                }
                else {
                    res.json("Success");
                }
            });
        }
    });
})


router.get('/getCounts', function (req, res) {

    ApplicationModel.countDocuments({}, function (err, result) {
        DLApplicationModel.countDocuments({}, function (err, result2) {
            DLEntries.countDocuments({}, function (err, result3) {
                res.json({ LL: result, DL: result2, DLEntry: result3 });
            });
        });
    });

})






module.exports = router;
