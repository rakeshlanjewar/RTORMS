const express = require('express')
var mongoose = require('mongoose');
var bodyParser = require("body-parser");
var path = require("path");

const uri = "mongodb+srv://rakesh:wis3Monster25@cluster0.rfg80.mongodb.net/RTORMS?retryWrites=true&w=majority";

mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });
const app = express()
const port = 4200
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  // we're connected!
  console.log(`Connected to database`)
});

app.use(express.static(path.join(__dirname,"..", 'build')));

// app.get('/*', (req, res) => {
//   res.sendFile(path.join(__dirname, 'build', 'index.html'));
// });


var insurance = require('./insurance');
app.use('/api/v1/insurance', insurance);

var applicationForm = require('./Applications');
app.use('/api/v1/applicationForm', applicationForm);


app.listen(process.env.PORT || port, () => console.log(`Example app listening on port ${port}!`))