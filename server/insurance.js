const express = require('express')
var mongoose = require('mongoose');
var router = express.Router();


router.use(function timeLog(req, res, next) {
  next();
});

var InsuranceSchema = new mongoose.Schema({
  ownerName: { type: String, required: true },
  vehicleNumber: { type: String, required: true },
  InsuranceDate: Date,
  ExpiryDate: { type: Date, required: true },
  mobNumber: Number,
  amount: Number,
  Commision: Number,
  company: String,
  vehClass: String,

}, { timestamps: true });

var InsuranceModel = mongoose.model('Insurance', InsuranceSchema);

router.post('/newinsurance', function (req, res) {
  var entry = new InsuranceModel(req.body);
  entry.save(function (err) {
    if (err) {
      res.status(500).json(err);
    }
    else {
      res.json("Saved");
    }
  });
})

router.delete('/deleteinsurance', function (req, res) {
  var id = req.body._id;
  InsuranceModel.deleteOne({ _id: id }, function (err) {
    if (err) {
      res.status(500).json("Failed");
    }
    else {
      res.json("Deleted");
    }
  });
})


router.post('/updateinsurance', function (req, res) {
  if (req.body.ExpiryDate == null || req.body.ExpiryDate == "") {
    return res.status(500).send("Failed");
  }

  var id = req.body._id;
  InsuranceModel.updateOne({ _id: id }, req.body, function (err) {
    if (err) {
      res.status(500).send("Failed");
    }
    else {
      res.json("Updated");
    }
  });
})


router.get('/getinsurances', function (req, res) {
  InsuranceModel.find({}, function (err, result) {
    if (err) {
      res.status(500).send("Failed");
    }
    res.json(result);
  });
})


function addDays(date, daysToAdd) {
  var _24HoursInMilliseconds = 86400000;
  return new Date(date.getTime() + daysToAdd * _24HoursInMilliseconds);
};

function minTwoDigits(n) {
  return (n < 10 ? '0' : '') + n;
}

function datetoString(date) {
  return date.getFullYear() + "-" + minTwoDigits(date.getMonth() + 1) + "-" + minTwoDigits(date.getDate())
}

router.get('/getInsuranceExpireCount', function (req, res) {
  var datetime = new Date();

  InsuranceModel.countDocuments({ ExpiryDate: { "$lte": datetoString(datetime) } }, function (err, result) {
    res.json({ Insurances: result });
  });
})


module.exports = router;
