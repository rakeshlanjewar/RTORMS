
module.exports = function (mongoose) {

    var LLApplicationSchema = new mongoose.Schema({
        appNo: { type: String, required: true },
        appDate: Date,
        name: { type: String, required: true, uppercase: true },
        mobNumber: Number,
        DOB: Date,
        slotdate: Date,
        type: String,
        rto: String,
        remark: String
    }, { timestamps: true });


    var DLApplicationSchema = new mongoose.Schema({
        appNo: { type: String },
        appDate: Date,
        name: { type: String, required: true, uppercase: true },
        mobNumber: Number,
        DOB: Date,
        slotdate: Date,
        type: String,
        llnumber: { type: String, uppercase: true },
        remark: String
    }, { timestamps: true });

    var dlEntrySchema = new mongoose.Schema({
        dlnumber: { type: String, required: true },
        dldate: Date,
        dlexpiry: Date,
        name: { type: String, required: true },
        mobNumber: Number
    }, { timestamps: true });


    var models = {
        ApplicationModel: mongoose.model('LLApplications', LLApplicationSchema),
        DLApplicationModel: mongoose.model('DLApplications', DLApplicationSchema),
        DLEntries: mongoose.model('DLEntries', dlEntrySchema)
    };

    return models;
}