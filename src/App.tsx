import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Home from "./components/home";
import NewLL from "./components/newLL";
import NewDL from "./components/newDL";
import DlEntry from "./components/dlEntry";
import Insurance from "./components/insurance";
import HomeIcon from '@material-ui/icons/Home';
import AssignmentIcon from '@material-ui/icons/Assignment';
import { Divider } from "@material-ui/core";
import Filter1Icon from '@material-ui/icons/Filter1';
import Filter2Icon from '@material-ui/icons/Filter2';
import Filter3Icon from '@material-ui/icons/Filter3';
import DriveEtaIcon from '@material-ui/icons/DriveEta';
import DirectionsCarIcon from '@material-ui/icons/DirectionsCar';
import Reports from "./components/reports";
import './styles.css';
import { Left } from "react-bootstrap/lib/Media";
// import '../node_modules bootstrap/dist/css/bootstrap.min.css';

// import { FormControl, Button, Form,Nav ,Navbar} from 'react-bootstrap';

const drawerWidth = 180;
const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginLeft: 180
  },
  toolbar: theme.mixins.toolbar
}));

export default function ClippedDrawer() {
  const classes = useStyles();

  return (
    <div>
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"></link>

      {/* <Navbar bg="primary" variant="dark">
        <Navbar.Brand href="#home">Navbar</Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link href="#home">Home</Nav.Link>
          <Nav.Link href="#features">Features</Nav.Link>
          <Nav.Link href="#pricing">Pricing</Nav.Link>
        </Nav>
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button variant="outline-light">Search</Button>
        </Form>
      </Navbar> */}


      

      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" noWrap>
            <DriveEtaIcon/> <span className="Iconstyle">RTO</span>
          </Typography>
        </Toolbar>
      </AppBar>
      <div className="Navigation">
      <Router>
        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <div className={classes.toolbar} />
          <List>
            <ListItem button key="Home" component={Link} to="/">
              <ListItemIcon>
                <HomeIcon />
              </ListItemIcon>
              <ListItemText primary="Home" />
            </ListItem>
            <ListItem button key="New LL" component={Link} to="/NewLL">
              <ListItemIcon>
                <Filter1Icon />
              </ListItemIcon>
              <ListItemText primary="New LL" />
            </ListItem>
            <ListItem button key="New DL" component={Link} to="/NewDL">
              <ListItemIcon>
                <Filter2Icon />
              </ListItemIcon>
              <ListItemText primary="New DL" />
            </ListItem>
            <ListItem button key="DL Entry" component={Link} to="/DlEntry">
              <ListItemIcon>
                <Filter3Icon />
              </ListItemIcon>
              <ListItemText primary="DL Entry" />
            </ListItem>
            <Divider />
            <ListItem button key="Insurance" component={Link} to="/Insurance">
              <ListItemIcon>
                <DirectionsCarIcon />
              </ListItemIcon>
              <ListItemText primary="Insurance" />
            </ListItem>
            <div>
            <Divider />
            <ListItem button key="Insurance" component={Link} to="/Reports">
              <ListItemIcon>
                <AssignmentIcon />
              </ListItemIcon>
              <ListItemText primary="Reports" />
            </ListItem>
            </div>
          </List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/NewLL">
              <NewLL />
            </Route>
            <Route path="/NewDL">
              <NewDL />
            </Route>
            <Route path="/DlEntry">
              <DlEntry />
            </Route>
            <Route path="/Insurance">
              <Insurance />
            </Route>
            <Route path="/Reports">
              <Reports />
            </Route>
            <Route path="/insurancesExpiring">
              <Reports />
            </Route>
          </Switch>
        </main>
      </Router>
      </div>
    </div>
  );
}
