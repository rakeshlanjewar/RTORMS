import React from "react";
import MaterialTable from "material-table";
import Swal from 'sweetalert2'


interface Row {
    _id: string;
    surname: string;
}

export default function TableProvider(props: {title:string, columns:any,action:any, get: RequestInfo; add: RequestInfo; update: RequestInfo; delete: RequestInfo; }) {
    const [rowdata, setData] = React.useState<Row[]>([])
    

    async function fetchData() {
        const data = await fetch(props.get).then((Response) => {
            if (Response.status !== 200) {
                alert("Failed to load table data");
                return;
            }
            return Response.json()
        }

        );
        setData(data);
    }


    React.useEffect(() => {
        fetchData();
    }, [])


    function showMessage(icon: "success" | "error" | "warning" | "info" | "question" | undefined,title: string,text:string){
        Swal.fire(title, text,icon)
    }

    return (
        <div>
            <MaterialTable
                title={props.title}
                columns={props.columns}
                data={rowdata}
                options={{
                    actionsColumnIndex: -1,
                    exportButton: true
                }}
                actions={props.action}
                editable={{
                    onRowAdd: newData =>
                        new Promise((resolve, reject) => {
                            return fetch(props.add,
                                {
                                    method: "post",
                                    body: JSON.stringify(newData),
                                    headers: { 'Content-Type': 'application/json' }
                                }).then(res => {
                                    if (res.status !== 200) {
                                        throw new Error("Failed to Load Data");
                                    }
                                    else {
                                        fetchData().then(() => {
                                            resolve();
                                            showMessage('success','Success !','Data has been saved successfully')
                                        });
                                    }
                                }).catch((error) => {
                                    showMessage('error','Error','Error has been occured while performing this operation')
                                    reject();
                                });
                        }),
                    onRowUpdate: (newData, oldData) =>
                        new Promise((resolve, reject) => {
                            return fetch(props.update,
                                {
                                    method: "post",
                                    body: JSON.stringify(newData),
                                    headers: { 'Content-Type': 'application/json' }
                                }).then(res => {
                                    if (res.status !== 200) {
                                        throw new Error("Failed to update the record");
                                    }
                                    else {
                                        fetchData().then(() => {
                                            resolve();
                                            showMessage('success','Success !','Record Has been updated')
                                        });
                                    }
                                }).catch(() => {
                                    showMessage('error','Error','Error has been occured while performing this operation')
                                    reject();
                                });

                        }),
                    onRowDelete: oldData =>
                        new Promise((resolve, reject) => {
                            return fetch(props.delete,
                                {
                                    method: "delete",
                                    body: JSON.stringify({ _id: oldData._id }),
                                    headers: { 'Content-Type': 'application/json' }
                                }).then(res => {
                                    if (res.status !== 200) {
                                        throw new Error("Failed to Delete the record");
                                    }
                                    else {
                                        fetchData().then(() => {
                                            resolve();
                                            showMessage('success','Success !','Record Has been deleted')
                                        });
                                    }
                                }).catch(() => {
                                    showMessage('error','Error','Error has been occured while performing this operation')
                                    reject();
                                });
                        })
                }}   
            />
        </div>
    );
}
