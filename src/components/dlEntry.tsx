import React from "react";
import TableProvider from './MySmartTable'

export default function DlEntry() {

  var apiRoot = '/api/v1/applicationForm';

  return (
    <TableProvider title='DL Entry' get={`${apiRoot}/getDLentries`} add={`${apiRoot}/newDL`}
      update={`${apiRoot}/updateDLentry`} delete={`${apiRoot}/deleteDLentry`}
      columns={[
        { title: "_id", field: "_id", hidden: true },
        { title: "Name", field: "name" },
        { title: "DL Number", field: "dlnumber" },
        { title: "Mobile Number", field: "mobNumber", type: "numeric" },
        { title: "DL Date", field: "dldate", type: "date" },
        { title: "DL Expiry", field: "dlexpiry", type: "date" },
      ]} action={[]}
    ></TableProvider>
  );
}
