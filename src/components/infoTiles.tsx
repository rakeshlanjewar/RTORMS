import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Paper, Typography, Link } from '@material-ui/core';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
    depositContext: {
        flex: 1,
    },
}));

function preventDefault(event: { preventDefault: () => void; }) {
    event.preventDefault();
}

export default function InfoTile(props: { title: React.ReactNode; count: React.ReactNode; linkText:React.ReactNode; link: string }) {
    const classes = useStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

    return (
        <Grid item xs={12} md={4} lg={3}>
            <Paper className={fixedHeightPaper}>
                <React.Fragment>
                    <Typography component="h2" variant="h6" color="primary" gutterBottom>
                        {props.title}
                    </Typography>
                    <Typography component="p" variant="h4">
                        {props.count}
                    </Typography>
                    <Typography color="textSecondary" className={classes.depositContext}>
                        With Date / Without Date
                    </Typography>
                    <div className="view-report">
                        <Link color="primary" href={props.link}>
                        {props.linkText}
                        </Link>
                    </div>
                </React.Fragment>
            </Paper>
        </Grid>
    );
}
