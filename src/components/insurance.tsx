import React from "react";
import TableProvider from './MySmartTable'


export default function Insurance() {

  var apiRoot = '/api/v1/insurance';

  return (
    <TableProvider title='Insurance' get={`${apiRoot}/getinsurances`} add={`${apiRoot}/newinsurance`}
      update={`${apiRoot}/updateinsurance`} delete={`${apiRoot}/deleteinsurance`}
      columns={[
        { title: "_id", field: "_id", hidden: true },
        { title: "Name", field: "ownerName" },
        { title: "Reg No", field: "vehicleNumber" },
        { title: "Mob. No.", field: "mobNumber", type: "numeric" },
        { title: "Amount", field: "amount", type: "numeric" },
        { title: "Comm.", field: "Commision", type: "numeric" },
        { title: "Expiry", field: "ExpiryDate", type: "date" },
        {
          title: "Company", field: "company", lookup: {
            1: 'DIGIT', 2: 'BHARTI', 3: 'SBI', 4: 'BAJAJ ALLINZ',
            9: 'NEW INDIA', 10: 'ICICI', 11: 'Oriental', 6: 'RELIANCE',
            5: 'IFFCO TOKIO', 7: 'TATA AIG', 8: 'CHOLA MS', 99: 'Other',
          }
        },
        {
          title: "Class", field: "vehClass", lookup: {
            1: 'Motor Cycle / Scooty ',
            2: 'Truck', 3: 'Car / Jeep / SUV', 4: 'Tracter/Trailer', 5: 'Mini Truck', 6: 'Other'
          }
        },
      ]}
      action={[]}
    ></TableProvider>
  );
}
