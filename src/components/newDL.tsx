import React from "react";
import TableProvider from './MySmartTable'
import Swal from 'sweetalert2'


export default function NewDL() {

  var apiRoot = '/api/v1/applicationForm';

  return (
    <TableProvider title='New Permanent' columns={[
      { title: "_id", field: "_id", hidden: true },
      { title: "Learning No.", field: "llnumber", cellStyle: { wordBreak: 'break-all' } },
      { title: "AppNo.", field: "appNo", cellStyle: { wordBreak: 'break-all', maxWidth: 10 } },
      { title: "App Date", field: "appDate", type: "date" },
      { title: "Name", field: "name", cellStyle: { wordBreak: 'break-all' } },
      { title: "Mob. Num.", field: "mobNumber", type: "numeric", cellStyle: { wordBreak: 'break-all' } },
      { title: "DOB", field: "DOB", type: "date" },
      { title: "Slot Date", field: "slotdate", type: "date" },
      { title: "Remark", field: "remark", cellStyle: { wordBreak: 'break-all' } }
    ]} get={`${apiRoot}/getDLApplications`} add={`${apiRoot}/newDLAppForm`}
      update={`${apiRoot}/updateDLApplication`} delete={`${apiRoot}/deleteDLApplication`} action={[
        {
          icon: 'send',
          tooltip: 'Save To DL Database',
          onClick: (event: any, rowData: any) => {
            console.log(rowData)

            Swal.mixin({
              input: 'text',
              confirmButtonText: 'Next &rarr;',
              showCancelButton: true,
              progressSteps: ['1', '2', '3']
            }).queue([
              {
                title: 'Question 1',
                text: 'Chaining swal2 modals is easy'
              },
              'Question 2',
              'Question 3'
            ]).then((result) => {
              if (result.value) {
                const answers = JSON.stringify(result.value)
                Swal.fire({
                  title: 'All done!',
                  html: `
                    Your answers:
                    <pre><code>${answers}</code></pre>
                  `,
                  confirmButtonText: 'Lovely!'
                })
              }
            })




            var dlnumber = prompt("Please Enter DL Number");
            var dldate = prompt("Please Enter Date of issue (dd-mm-yyyy)");
            var dlexpiry = prompt("Please Enter Date of Expiry (dd-mm-yyyy)");

          if(dlnumber === null || dlnumber === ''){
            return;
          }
            
          rowData.dlnumber = dlnumber;
          rowData.dldate = dldate;
          rowData.dlexpiry = dlexpiry;

          console.log(rowData)
          fetch(`${apiRoot}/DLtoDLEntry`,
            {
                method: "post",
                body: JSON.stringify(rowData),
                headers: { 'Content-Type': 'application/json' }
            }).then(res => {
                if (res.status !== 200) {
                    throw new Error("Failed to Load Data");
                }
                else {
                    alert("Data Moved Sucessfully");
                    window.location.href = "/DlEntry";
                }
            }).catch(() => {
                alert("Failed to Perform the operations");
            });
          }
        },
      ]}
    ></TableProvider>
  );
}
