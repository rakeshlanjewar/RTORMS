import React from "react";
import TableProvider from './MySmartTable'
import Swal from 'sweetalert2'
import { useHistory } from "react-router-dom";


export default function NewLL() {
  const history = useHistory();

  var apiRoot = '/api/v1/applicationForm';

  return (
    <TableProvider title='New Learning' columns={[
      { title: "_id", field: "_id", hidden: true },
      { title: "AppNo.", field: "appNo", cellStyle: { wordBreak: 'break-all', maxWidth: 10 } },
      { title: "App Date", field: "appDate", type: "date" },
      { title: "Name", field: "name", cellStyle: { wordBreak: 'break-all' } },
      { title: "Mob. Num.", field: "mobNumber", type: "numeric", cellStyle: { wordBreak: 'break-all' } },
      { title: "DOB", field: "DOB", type: "date" },
      { title: "Slot Date", field: "slotdate", type: "date" },
      {
        title: "RTO", field: "rto", initialEditValue: 1,
        lookup: {
          1: 'Pune',
          2: 'Kalyan',
          3: 'Thane',
          4: 'Mumbai',
          5: 'Pimpri',
          6: 'Nagpur',
          7: 'Other'
        }
      },
      { title: "Remark", field: "remark", cellStyle: { wordBreak: 'break-all' } }
    ]}  get={`${apiRoot}/getApplications`} add={`${apiRoot}/newAppForm`}
    update={`${apiRoot}/updateApplication`} delete={`${apiRoot}/deleteApplication`} action={[
      {
        icon: 'send',
        tooltip: 'Proceed To DL',
        onClick:(event: any, rowData: any)=> {
         
          Swal.fire({
            title: 'Please Enter LL Number',
            input: 'text',
            inputAttributes: {
              autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Proceed to DL',
            showLoaderOnConfirm: true,
            preConfirm: (llnumber) => {
              rowData.llnumber = llnumber;
              return fetch(`${apiRoot}/LLtoDL`,{
                        method: "post",
                        body: JSON.stringify(rowData),
                        headers: { 'Content-Type': 'application/json' }
                    })
                .then(response => {
                  if (!response.ok) {
                    throw new Error(response.statusText)
                  }
                  return response.json()
                })
                .catch(error => {
                  Swal.showValidationMessage(`${error}`)
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
          }).then((result) => {
            if (result.value) {
              Swal.fire(
                'Good job!',
                'Data Moved Successfully',
                'success'
              )
              history.push("/newDL");
            }
          })

        //   var llnumber = prompt("Please Enter LL Number");
        //   if(llnumber === null || llnumber === ''){
        //     return;
        //   }
            
        //   rowData.llnumber = llnumber;
        //   console.log(rowData)
        //   fetch(`${apiRoot}/LLtoDL`,
        //     {
        //         method: "post",
        //         body: JSON.stringify(rowData),
        //         headers: { 'Content-Type': 'application/json' }
        //     }).then(res => {
        //         if (res.status !== 200) {
        //             throw new Error("Failed to Load Data");
        //         }
        //         else {
        //             alert("Data Moved Sucessfully");
        //             window.location.href = "/NewDL";
        //         }
        //     }).catch(() => {
        //         alert("Failed to Perform the operations");
        //     });

        }
      },
    ]}
    ></TableProvider>
  );
}
