import React from 'react';
import { Grid } from '@material-ui/core';
import InfoTile from './infoTiles'

interface CountData {
  LL: string;
  DL: string;
  DLEntry:string;
  Insurances:string;
}



export default function Reports() {
  const [cData, setData] = React.useState<CountData>()
  const [insuData, setData2] = React.useState<CountData>()

  async function fetchData() {
    //let url = '/api/v1/dlEntries/getDLentries'
    const data = await fetch('/api/v1/applicationForm/getCounts').then((Response) => {
      if (Response.status !== 200) {
        alert("Failed to load table data");
        return;
      }
      return Response.json()
    }

    );
    setData(data);
  }

  async function fetchData2() {
    //let url = '/api/v1/dlEntries/getDLentries'
    const data = await fetch('/api/v1/insurance/getInsuranceExpireCount').then((Response) => {
      if (Response.status !== 200) {
        alert("Failed to load table data");
        return;
      }
      return Response.json()
    }

    );
    setData2(data);
  }

  React.useEffect(() => {
    fetchData();
    fetchData2();
  }, [])

  return (

    <Grid container spacing={3}>
      <InfoTile title="Insurances Expiring" count={cData?.LL} linkText="View Report" link="/insurancesExpiring"></InfoTile>
      <InfoTile title="Insurances Expired"count={cData?.DL} linkText="View Report" link="/insurancesExpiring"></InfoTile>
      <InfoTile title="DL Entries" count={cData?.DLEntry} linkText="View Report" link="/insurancesExpiring"></InfoTile>
      <InfoTile title="Insurances Expiring" count={insuData?.Insurances} linkText="View Report" link="/insurancesExpiring"></InfoTile>
    </Grid>

  );
}
